# BDD: Mise en relation Patient/Aide à domicile  

## Utilisation de la BDD

Le projet utilise MariaDB sur un serveur linux (disponible pour une durée limité)

### Utilisation sur le serveur dédié
Il est possible de se connecter en ssh sur le serveur en suivant le protocol ci dessous :

> A lancer dans le shell windows ([WIN]+[R] puis taper cmd)

```shell
$ ssh pi@92.148.234.176
```

> Mot de passe : 14785 puis saisir les commandes suivantes :

```shell
$ cd Documents/BDD_projects/
$ sudo mysql -u root
```

> Dans mariaDB saisir la commande suivante :

```shell
$ source Creation.sql
```

### Utilisation sur un autre serveur avec MariaDB installé

> Télécharger le dépot puis dans mariaDB saisir la commande suivante :

```shell
$ source Creation.sql
```
