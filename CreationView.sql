
CREATE VIEW `Patient` AS
SELECT Accounts.login,Users.firstName,Users.lastName,Users.gender,Users.birthDate,CONCAT(Address.streetNo,' ',Address.streetLocation,',',Address.building,',',Address.floor,',',Address.postaleNumber,' ',Address.postalName) AS "address", Contacts.email,Contacts.mobile,Contacts.landline,Contacts.fax FROM Accounts
INNER JOIN Users ON Accounts.idAccount = Users.FK_idAccount
INNER JOIN Address ON Users.idUsers = Address.FK_idUsers
INNER JOIN Contacts ON Users.idUsers = Contacts.FK_idUsers;
SELECT * FROM Patient LIMIT 5;

-- TODO
/*
CREATE VIEW `Clinic` AS
SELECT
*/
