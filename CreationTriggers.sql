
DELIMITER |
CREATE TRIGGER InitialiseLog AFTER INSERT
ON Accounts FOR EACH ROW
BEGIN
    INSERT INTO ConnexionsHistory (dateConnexion,FK_idAccount) VALUES (NOW(),new.idAccount);
END |

DELIMITER ;

/*
CREATE TRIGGER UpdateAddressDate BEFORE UPDATE
ON Address FOR EACH ROW
BEGIN
	UPDATE Address SET dateLastModification = NOW() 
	WHERE idAddress= NEW.idAddress; 
END |

DELIMITER ;
*/

