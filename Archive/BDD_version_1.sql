DROP DATABASE IF EXISTS HealthProject;

CREATE DATABASE HealthProject;
USE HealthProject;


DROP TABLE IF EXISTS Services;
CREATE TABLE Services(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50),
	PRIMARY KEY(id)
);

DROP TABLE IF EXISTS ContactDetails;
CREATE TABLE ContactDetails
(
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	city varchar(50),
	address varchar(50),
	phoneNumber varchar(20),
	email varchar(20)
);

DROP TABLE IF EXISTS Clinics;
CREATE TABLE Clinics
(
	id int AUTO_INCREMENT PRIMARY KEY,
	name varchar(50) NOT NULL,
	iban varchar(27),
	idContactDetails int,
	FOREIGN KEY(idContactDetails) REFERENCES ContactDetails(id)
);

DROP TABLE IF EXISTS Professions;
CREATE TABLE Professions
(
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name varchar(20) NOT NULL
);

DROP TABLE IF EXISTS ServiceProviders;
CREATE TABLE ServiceProviders
(
	id int AUTO_INCREMENT PRIMARY KEY,
	name varchar(50) NOT NULL,
	firstName varchar(50),
	actionRange double,
	firstConnection date,
	lastConnection date,
	idProfession int,
	idContactDetails int,
	FOREIGN KEY(idProfession) REFERENCES Professions(id),
	FOREIGN KEY(idContactDetails) REFERENCES ContactDetails(id)
);

DROP TABLE IF EXISTS Clients;
CREATE TABLE Clients
(
	id int AUTO_INCREMENT PRIMARY KEY,
	name varchar(50),
	firstName varchar(50),
	firstConnection date,
	lastConnection date,
	idContactDetails int,
	FOREIGN KEY(idContactDetails) REFERENCES ContactDetails(id)
);

DROP TABLE IF EXISTS ProvidedServices ;
CREATE TABLE ProvidedServices
(
        id int AUTO_INCREMENT PRIMARY KEY,
        idServiceProvider int,
        idService int,
        price int,
        FOREIGN KEY(idServiceProvider) REFERENCES ServiceProviders(id),
        FOREIGN KEY(idService) REFERENCES Services(id)
);

DROP TABLE IF EXISTS Quotations;
CREATE TABLE Quotations
(
        id INT AUTO_INCREMENT PRIMARY KEY,
	creationDate date NULL,
        idClient int,
        idService int,
        FOREIGN KEY(idClient) REFERENCES Clients(id),
        FOREIGN KEY(idService) REFERENCES Services(id)
);


-- REMPLISSAGE DES TABLES

INSERT INTO Services (name) VALUES ('Pansement');
INSERT INTO Services (name) VALUES ('Nettoyage');
INSERT INTO Services (name) VALUES ('Repassage'); 
INSERT INTO Services (name) VALUES ('Toiletage');
	
INSERT INTO Professions (name) VALUES ("Aide Soignant");
INSERT INTO Professions (name) VALUES ("Infirmière");
INSERT INTO Professions (name) VALUES ("Sage Femme"); 
INSERT INTO Professions (name) VALUES ("Kiné");

INSERT INTO ContactDetails (city,address,phoneNumber,email) VALUES ("Paris","29 Rue du Louvre","0600000000","BruceWillis@wanado.fr");
INSERT INTO ContactDetails (city,address,phoneNumber,email) VALUES ("Paris","25 Rue de la grand tour effeil","0600000001","LeonardoDicaprio@wanado.fr");
INSERT INTO ContactDetails (city,address,phoneNumber,email) VALUES ("Strasbourg","07 Rue de la cathedral","0600000002","BernardTapis@wanado.fr");
INSERT INTO ContactDetails (city,address,phoneNumber,email) VALUES ("Nice","29 Promenade des anglais","0600000003","Macron@wanado.fr");
INSERT INTO ContactDetails (city,address,phoneNumber,email) VALUES ("Lyon","3 Avenue de la traboule","0600000004","KevinDu69@wanado.fr"),
("Montpellier", "12 rue des cadenas", "0600000006", "abcde@hotmail.fr"),
("Saverne", "9 Boulevard Bronson", "0600000007", "fghij@gmail.com"),
("Bordeaux", "2 Impasse des borgnes", "0600000008", "klmno@yahoo.fr"),
('Marseille', '2 rue des Tulipes', '0628456892', 'contact@abrapa.fr'),
('Bordeaux', '47 batiment bleu', '0435607288', 'epicuria@contact.fr');
	
INSERT INTO Clients (name,firstName,firstConnection, lastConnection, idContactDetails) VALUES 
("Willis", "Bruce", date(now()), date(now()), 1),
("DiCaprio", "Leonardo", date(now()), date(now()), 2),
("Tapis", "Bernard", date(now()), date(now()), 3),
("Macron", "Manu", date(now()), date(now()), 4),
("Cassoulet", "Kevin", date(now()), date(now()), 5);

INSERT INTO ServiceProviders
(name, firstName, idProfession,idContactDetails,ActionRange,firstConnection,lastConnection)
VALUES ("Robin", "Murielle", 1, 6, 10, date(now()), date(now())),
("Bernardinho", "Marguerite" ,1 ,7 ,10, date(now()), date(now())),
("Colomb", "Patrick", 2, 8, 10, date(now()), date(now()));

INSERT INTO Clinics (name, iban, idContactDetails) VALUES 
("Abrapa", "FR9312739000704531418449R13", 9),
("Epicuria", "FR2210096000308954516697U18", 10);

--INSERT INTO ProvidedServices(price, idServiceProvider, idService)
INSERT INTO ProvidedServices(idService, idServiceProvider, price) VALUES
(1,1 ,10 ),
(1,2 ,15 ),
(1,3 ,20 ),
(2,1,12),
(2,2,17),
(2,3,25),
(3,1,9),
(3,2,14),
(3,3,19);


INSERT INTO Quotations(creationDate, idClient, idService) VALUES
(date(now()), 1, 1),
(date(now()), 2, 2),
(date(now()), 5, 3);


SHOW WARNINGS;
