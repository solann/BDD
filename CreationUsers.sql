-- ///////////////////////// PATIENT ////////////////////////

DROP USER IF EXISTS UserWritter@'%';
CREATE USER UserWritter@'%' IDENTIFIED BY '123456789';
GRANT INSERT ON Accounts TO UserWritter@'%';
GRANT INSERT ON Users TO UserWritter@'%';
GRANT INSERT ON Address TO UserWritter@'%';
GRANT INSERT ON Contacts TO UserWritter@'%';

DROP USER IF EXISTS UserUpdater@'%';
CREATE USER UserUpdater@'%' IDENTIFIED BY '123456789';
GRANT UPDATE ON Accounts TO UserUpdater@'%';
GRANT UPDATE ON Users TO UserUpdater@'%';
GRANT UPDATE ON Address TO UserUpdater@'%';
GRANT UPDATE ON Contacts TO UserUpdater@'%';

GRANT EXECUTE ON PROCEDURE CreatePatient TO 'UserWritter'@'%';

-- ///////////////////////  BUSINESS //////////////////////

DROP USER IF EXISTS BusinessWritter@'%';
CREATE USER BusinessWritter@'%' IDENTIFIED BY '1234567989';
GRANT INSERT ON Accounts TO BusinessWritter@'%';
GRANT INSERT ON Business TO BusinessWritter@'%';
GRANT INSERT ON ServiceProvider TO BusinessWritter@'%';

DROP USER IF EXISTS BusinessUpdater@'%';
CREATE USER BusinessUpdater@'%' IDENTIFIED BY '123456789';
GRANT INSERT,UPDATE ON ActionsRanges TO BusinessUpdater@'%';
GRANT INSERT,UPDATE On gpsPoints TO BusinessUpdater@'%';
GRANT INSERT,UPDATE ON PolygonalsActionsRanges TO BusinessUpdater@'%';

GRANT EXECUTE ON PROCEDURE CreateIndependentBusiness TO 'BusinessWritter'@'%';
GRANT EXECUTE ON PROCEDURE AddActionRange TO 'BusinessUpdater'@'%';
GRANT EXECUTE ON PROCEDURE AddPolygonalsActionsRanges TO 'BusinessUpdater'@'%';


