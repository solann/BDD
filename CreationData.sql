
-- Populate the 'enumeration' table
CALL InitialiseBasicValue();

-- Create several patient
CALL CreatePatient('LoginDeLouis','*****','Louis','Friedrich','1995-05-30','Male','12','Chemin du king','Bat A.', '3ème étage','67000','Bas-Rhin','loulou@hotmail.fr','0400000000','0600000000','0300000000');
CALL CreatePatient('LoginDeSolann','***jkl**','Solann','Puygrenier','1994-11-11','Male','5','Chemin de la banane','batiment D14', '5ème étage','69100','Villeurbane','soso@hotmail.fr','0400000000','0600000000','0300000000');
CALL CreatePatient('LoginDeBernard','*hj****','Bernard','Banam','1990-10-30','Male','8','Avenue de la queen','A', '','67000','Bas-Rhin','unMail@hotmail.fr','0403333333','3638333333','3333333333');
CALL CreatePatient('LoginDeRoger','****lkjh*','Roger','Yojimbo','1998-01-11','Female','10','rue du general petit','C808', '','69100','Villeurbane','unMail@hotmail.fr','7477477777','7677777777','7377777777');

-- Create profesional with circular area of work
CALL CreateIndependentBusiness('LogPro','****','Aide soignant','Meyer health','73282932000074','Bernard','Le Duc d\'alsace','1980-12-01','Male','5','Rue de la liberté','','RDC','06800','Cagnes-sur-mers','duc@wanadoo.fr','0400040404','','');
CALL AddActionRange(1,"10",'48.85341','2.3488');

-- Create profesional with polygonale area of work
CALL CreateIndependentBusiness('LogProV2','****','Aide soignant','Iron Health','7898865420450645','Jean','blond blond','1990-12-01','Male','3','Rue de la santé','','1er','01000','Ains','Iron@health.fr','0606060606','','');
CALL AddPolygonalsActionsRanges(2,0,'48.85000','2.3000');
CALL AddPolygonalsActionsRanges(2,1,'48.86000','2.3400');
CALL AddPolygonalsActionsRanges(2,2,'48.90000','2.3900');

-- Create another profesional with polygonale area of work
CALL CreateIndependentBusiness('tdk','**fdz*','Aide soignant','Aggresiv concurent','0898987361050645','James','bond','1999-08-20','Male','108','Rue de la croix','Batimen C','2nd','06000','Nice','bgdu69@hotmail.fr','0704090605','','');
CALL AddPolygonalsActionsRanges(3,0,'50.0','50.00');
CALL AddPolygonalsActionsRanges(3,1,'51.0','52.12');
CALL AddPolygonalsActionsRanges(3,2,'55.0','58.80');

-- Create one appointement for today with two practician proposition and validate one of them
CALL CreateAppointementRequest(1,1,DATE(NOW()));
CALL AddAppointementContents(1,"Ménage");
CALL AddAppointementContents(1,"Faire les repas");
CALL AddAppointementContents(1,"Aide à la toilette");
INSERT INTO AppointementsProposal (FK_idAppointementsRequest, FK_idServiceProvider) VALUES (1,1);
INSERT INTO AppointementsProposal (FK_idAppointementsRequest, FK_idServiceProvider) VALUES (1,2);
CALL ValidateAppointement(1,2);

-- Create one appointement later with two practician proposition and cancel one of them
CALL CreateAppointementRequest(2,1,'2021-12-12 08:30:00');
CALL AddAppointementContents(2,"Ménage");
INSERT INTO AppointementsProposal (FK_idAppointementsRequest, FK_idServiceProvider) VALUES (1,1);
INSERT INTO AppointementsProposal (FK_idAppointementsRequest, FK_idServiceProvider) VALUES (1,2);
CALL CancelAppointement(2);
