SELECT 'Nom des participants et heure des rdv validés dans les dernières 24h';
SELECT pro.lastName, u.lastName, r.meetingDateTime
FROM Users u, AppointementsValidated v, AppointementsProposal p, AppointementsRequest r, ServiceProvider pro
WHERE u.idUsers = v.FK_idUsers
AND v.idAppointementsValidated = p.idAppointementsProposal
AND p.FK_idAppointementsRequest = r.idAppointementsRequest
AND p.FK_idServiceProvider = pro.idServiceProvider
AND v.dateValidationRequest >= DATE_SUB(NOW(), INTERVAL 1 DAY);


SELECT ' Les clients ayant déjà consulté un kiné';
select u.lastName, r.meetingDateTime
from Users u, AppointementsValidated v, AppointementsProposal p, AppointementsRequest r, ServiceProvider pro, Professions prof
where u.idUsers = v.FK_idUsers
and v.idAppointementsValidated = p.idAppointementsProposal
and p.FK_idAppointementsRequest = r.idAppointementsRequest
and p.FK_idServiceProvider = pro.idServiceProvider
and pro.FK_idProfessions = prof.idProfessions
and r.meetingDateTime <= now();

select 'Nb de consultations de chaque client chez un kiné';
select u.lastName, count(u.idUsers)
from Users u, AppointementsValidated v, AppointementsProposal p, AppointementsRequest r, ServiceProvider pro, Professions prof
where u.idUsers = v.FK_idUsers
and v.idAppointementsValidated = p.idAppointementsProposal
and p.FK_idAppointementsRequest = r.idAppointementsRequest
and p.FK_idServiceProvider = pro.idServiceProvider
and pro.FK_idProfessions = prof.idProfessions
group by u.idUsers;

--select 'Nb d\'aide soignants par département';



select 'nb de clients dans le département 67';
select count(u.idUsers)
from Users u, Address a
where u.idUsers = a.idAddress
and a.postaleNumber like '67%';

select 'Nom et profession de chaque collaborateur de l\'agence id 1';
select pro.lastName, prof.title
from Professions prof, ServiceProvider pro, Business b
where b.idBusiness = 1
and b.idBusiness = pro.FK_idBusiness
and pro.FK_idProfessions = prof.idProfessions;

select 'Les prestations proposées par le praticien id 2';
select prest.type
from Prestations prest, Professions prof, ServiceProvider pro
where pro.idServiceProvider = 2
and pro.FK_idProfessions = prof.idProfessions
and prof.idProfessions = prest.FK_idProfessions;

select 'L\'agence ayant eu le plus de rdv en 2021';
select b.businessName, count(b.idBusiness)
from Business b, ServiceProvider pro, AppointementsProposal prop, AppointementsValidated v
where b.idBusiness = pro.FK_idBusiness
and pro.idServiceProvider = prop.FK_idServiceProvider
and prop.idAppointementsProposal = v.FK_idAppointementsProposal
group by b.idBusiness;

select 'Les coordonnées des praticiens proposant le soin 3';
select pro.lastName, c.email, c.mobile
from Contacts c, ServiceProvider pro, Professions prof, Prestations p
where c.FK_idServiceProvider = pro.idServiceProvider
and pro.FK_idProfessions = prof.idProfessions
and prof.idProfessions = p.FK_idProfessions
and p.idPrestations = 3;
