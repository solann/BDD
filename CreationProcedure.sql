

-- -----------------------------------------------------
-- Procedure (aid for creation of data)
-- -----------------------------------------------------
DROP PROCEDURE IF EXISTS InitialiseBasicValue;
DROP PROCEDURE IF EXISTS CreatePatient;
DROP PROCEDURE IF EXISTS CreateIndependentBusiness;
DROP PROCEDURE IF EXISTS AddActionRange;
DROP PROCEDURE IF EXISTS AddPolygonalsActionsRanges;
DROP PROCEDURE IF EXISTS CreateAppointementRequest;
DROP PROCEDURE IF EXISTS AddAppointementContents;
DROP PROCEDURE IF EXISTS ValidateAppointement;
DROP PROCEDURE IF EXISTS CancelAppointement;

DROP TRIGGER IF EXISTS InitialiseLog ;

DELIMITER |

/* Add the basic profession and associate the correct prestation */
CREATE PROCEDURE InitialiseBasicValue()
BEGIN
    INSERT INTO Professions (title) VALUES
      ("Aide soignant"),
      ("Infirmier"),
      ("Sage femme"),
      ("Kiné");
    INSERT INTO Prestations(type,FK_idProfessions) VALUES
      ("Ménage",1),
      ("Repassage",1),
      ("Faire les courses",1),
      ("Faire les repas",1),
      ("Aide à la toilette",1),
      ("Papysitting",1),
      ("Changement bandage",2),
      ("etc...",2),
      ("Consultation",3),
      ("Massage",4);
  INSERT INTO StatusRequest(descritpion) VALUES
    ("In progress"),
    ("Validated"),
    ("Cancel");

END |

/**/
CREATE PROCEDURE CreatePatient (_login VARCHAR(50), _password VARCHAR(100),
                               _firstName VARCHAR(45) ,_lastName VARCHAR(45) ,_birthDate DATE ,_gender ENUM('Male', 'Female'),
                               _streetNo VARCHAR(10), _streetLocation VARCHAR(200) ,_building VARCHAR(45),_floor VARCHAR(45) ,_postaleNumber VARCHAR(5) ,_postalName VARCHAR(45),
                               _email VARCHAR(270),_landline VARCHAR(20) ,_mobile VARCHAR(20),_fax VARCHAR(20))
BEGIN
    INSERT INTO Accounts (login,password) VALUES (_login,_password);
    SELECT idAccount INTO @idAccount FROM Accounts WHERE login = _login ;
    INSERT INTO Users (firstName,lastName,birthDate,gender,FK_idAccount) VALUES (_firstName,_lastName,_birthDate,_gender,@idAccount);
    SELECT idUsers INTO @idUsers FROM Users WHERE FK_idAccount = @idAccount;
    INSERT INTO Address (streetNo ,streetLocation,building,floor,postaleNumber,postalName,FK_idUsers) VALUES (_streetNo ,_streetLocation,_building,_floor,_postaleNumber,_postalName,@idUsers);
    INSERT INTO Contacts (email,landline,mobile,fax,FK_idUsers) VALUES(_email ,_landline ,_mobile,_fax,@idUsers);
END |


/* */
CREATE PROCEDURE CreateIndependentBusiness(_login VARCHAR(50), _password VARCHAR(100), _titleProfessions VARCHAR(45),
                                           _businessName VARCHAR(45) ,_siretNumber VARCHAR(45),
                                           _firstName VARCHAR(45) ,_lastName VARCHAR(45) ,_birthDate DATE ,_gender ENUM('Male', 'Female'),
                                           _streetNo VARCHAR(10), _streetLocation VARCHAR(200) ,_building VARCHAR(45),_floor VARCHAR(45) ,_postaleNumber VARCHAR(5) ,_postalName VARCHAR(45),
                                           _email VARCHAR(270),_landline VARCHAR(20) ,_mobile VARCHAR(20),_fax VARCHAR(20))
BEGIN
  INSERT INTO Accounts (login,password) VALUES (_login,_password);
  SELECT idAccount INTO @idAccount FROM Accounts WHERE login = _login;
  INSERT INTO Business (businessName,siretNumber,FK_idAccount) VALUES (_businessName,_siretNumber,@idAccount);
  SELECT idBusiness INTO @idBusiness FROM Business WHERE FK_idAccount = @idAccount;
  SELECT idProfessions INTO @idProfessions FROM Professions WHERE title = _titleProfessions;
  INSERT INTO ServiceProvider (firstName,lastName,birthDate,gender,FK_idBusiness,FK_idProfessions) VALUES (_firstName,_lastName,_birthDate,_gender,@idBusiness,@idProfessions);
END |

/* */
CREATE PROCEDURE AddActionRange(_idServiceProvider INT, _diameter FLOAT , _latitude DOUBLE, _longitude DOUBLE)
BEGIN
  INSERT INTO ActionsRanges (diameter,FK_idServiceProvider) VALUES (_diameter,_idServiceProvider);
  SELECT idActionsRanges INTO @idActionRange FROM ActionsRanges WHERE FK_idServiceProvider = _idServiceProvider;
  INSERT INTO gpsPoints(latitude,longitude,FK_idActionsRanges) VALUES (_latitude,_longitude,@idActionRange) ;
END |

/* */
CREATE PROCEDURE AddPolygonalsActionsRanges(_idServiceProvider INT,_indexGPS INT, _latitude DOUBLE, _longitude DOUBLE)
BEGIN
  INSERT INTO PolygonalsActionsRanges (FK_idServiceProvider,indexGPS) VALUES (_idServiceProvider,_indexGPS);
  SELECT idPolygonalsActionsRanges INTO @idPolygonalsActionsRanges FROM PolygonalsActionsRanges WHERE FK_idServiceProvider = _idServiceProvider AND indexGPS = _indexGPS;
  INSERT INTO gpsPoints(latitude,longitude,FK_idPolygonalsActionsRanges) VALUES (_latitude,_longitude,@idPolygonalsActionsRanges) ;
END |

/* */
CREATE PROCEDURE CreateAppointementRequest(_idUsers INT,_occurence INT, _meetingDateTime DATETIME)
BEGIN
  INSERT INTO AppointementsRequest (FK_idUsers,dateCreationRequest,occurence,meetingDateTime,FK_idStatusRequest) VALUES (_idUsers,DATE(NOW()),_occurence,_meetingDateTime,1);
END |

/* */
CREATE PROCEDURE AddAppointementContents(_idAppointmentsRequest INT,_type VARCHAR(45))
BEGIN
  SELECT idPrestations INTO @idPrestations FROM Prestations WHERE type = _type;
  INSERT INTO AppointementsContents (FK_idAppointmentsRequest,FK_idPrestations) VALUES (_idAppointmentsRequest,@idPrestations);
END |

/* */
CREATE PROCEDURE ValidateAppointement(_idAppointementsRequest INT,_idAppointementsProposal INT)
BEGIN
  UPDATE AppointementsRequest
  SET FK_idStatusRequest = 2
  WHERE idAppointementsRequest = _idAppointementsRequest;

  INSERT INTO AppointementsValidated (FK_idUsers,FK_idAppointementsProposal,dateValidationRequest)
    SELECT idAppointementsProposal,AppointementsRequest.FK_idUsers,DATE(NOW())  FROM AppointementsProposal
    INNER JOIN AppointementsRequest ON AppointementsRequest.idAppointementsRequest = FK_idAppointementsRequest
    WHERE idAppointementsProposal = _idAppointementsProposal;

END |

/* */
CREATE PROCEDURE CancelAppointement(_idAppointementsRequest INT)
BEGIN
  UPDATE AppointementsRequest
  SET FK_idStatusRequest = 3
  WHERE idAppointementsRequest = _idAppointementsRequest;

  DELETE FROM AppointementsProposal WHERE FK_idAppointementsRequest = _idAppointementsRequest;

END |

DELIMITER ;
